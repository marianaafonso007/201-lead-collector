package com.br.leadcollector.LeadCollector.enums;

public enum TipoDeLead {
    QUENTE(1),
    ORGANICO(2),
    FRIO(3);

    public int valor;

    public int getValor() {
        return valor;
    }
    TipoDeLead(int valor){
        this.valor = valor;
    }
}
