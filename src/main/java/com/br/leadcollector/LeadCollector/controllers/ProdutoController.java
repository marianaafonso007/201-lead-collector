package com.br.leadcollector.LeadCollector.controllers;

import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping ("/produtos")
public class ProdutoController {
    @Autowired
    private ProdutoService produtoService;

    @GetMapping
    public Iterable<Produto> buscarTodosProdutos(){
        return produtoService.buscarTodosProdutos();
    }

    @GetMapping("/{id}")
    public Produto buscarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarProdutoPorId(id);

        if (produtoOptional.isPresent()){
            return produtoOptional.get();
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID Não localizado.");
        }
    }

    @PostMapping()
    public ResponseEntity<Produto> salvarProduto(@RequestBody Produto produto){
        Produto produtoObjeto = produtoService.salvarProduto(produto);
        return ResponseEntity.status(201).body(produtoObjeto);
    }

    @PutMapping("/{id}")
    public Produto atualizarProduto(@PathVariable Integer id, @RequestBody Produto produto){
        produto.setId(id);
        return produtoService.atualizarProduto(produto);
    }

    @DeleteMapping("/{id}")
    public Optional<Produto>  deletarProduto(@PathVariable Integer id){
        Optional<Produto> produtoOptional = produtoService.buscarProdutoPorId(id);
        if (produtoOptional.isPresent()) {
            produtoService.deletarProduto(produtoOptional.get());
            return produtoOptional;
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}

