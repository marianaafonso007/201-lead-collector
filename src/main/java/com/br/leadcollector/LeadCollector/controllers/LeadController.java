package com.br.leadcollector.LeadCollector.controllers;

import com.br.leadcollector.LeadCollector.models.Lead;
import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/leads")
public class LeadController {
    @Autowired
    private LeadService leadService;

    @GetMapping
    public Iterable<Lead> buscarTodosLeads(){
        return  leadService.buscarTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);

        if (leadOptional.isPresent()){
            return leadOptional.get();
        }else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "ID Não localizado.");
        }
    }

    @PostMapping() //resposta da API com a classe Response Entity, permitindo inserir status e um corpo! ;)
    public ResponseEntity<Lead> salvarLead(@RequestBody @Valid Lead lead){
        List<Integer> produtosId = new ArrayList<>();

        for(Produto produto : lead.getProdutos()){
            produtosId.add(produto.getId());
        }

        Iterable<Produto> produtosIterable = leadService.buscarTodosProdutos(produtosId);
        lead.setProdutos((List) produtosIterable);
        Lead leadObjeto = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(leadObjeto);
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable Integer id, @RequestBody Lead lead){
        lead.setId(id);
        Lead leadObjeto = leadService.atualizarLead(lead);
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Lead> deletarLead(@PathVariable Integer id){
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()) {
            leadService.deletarLead(leadOptional.get());
            return ResponseEntity.status(200).body(leadOptional.get());
        } else {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
