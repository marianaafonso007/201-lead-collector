package com.br.leadcollector.LeadCollector.services;

import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProdutoService {
    @Autowired
    private ProdutoRepository produtoRepository;

    public Optional<Produto> buscarProdutoPorId(int id){
        return produtoRepository.findById(id);
    }

    public Produto salvarProduto(Produto produto){
        return produtoRepository.save(produto);
    }

    public Iterable<Produto> buscarTodosProdutos(){
        return produtoRepository.findAll();
    }

    public Produto atualizarProduto(Produto produto){
        Optional<Produto> produtoOptional = buscarProdutoPorId(produto.getId());

        if(produtoOptional.isPresent()){
            Produto produtoData = produtoOptional.get();
            if (produto.getNome() == null){
                produto.setNome(produtoData.getNome());
            }
            if (produto.getDescricao() == null){
                produto.setDescricao(produtoData.getDescricao());
            }
            if (produto.getPreco() == null){
                produto.setPreco(produtoData.getPreco());
            }
        }
        return produtoRepository.save(produto);
    }

    public void deletarProduto(Produto produto){
        produtoRepository.delete(produto);
    }

}
