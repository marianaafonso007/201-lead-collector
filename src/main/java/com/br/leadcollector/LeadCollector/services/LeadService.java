package com.br.leadcollector.LeadCollector.services;

import com.br.leadcollector.LeadCollector.models.Lead;
import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.repositories.LeadRepository;
import com.br.leadcollector.LeadCollector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private ProdutoRepository produtoRepository;

    public  Iterable<Produto> buscarTodosProdutos(List<Integer> produtosId){
        return produtoRepository.findAllById(produtosId);
    }

    public Optional<Lead> buscarPorId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);
        return leadOptional;
    }

    public Lead salvarLead(Lead lead) {
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads(){
        return leadRepository.findAll();
    }

    public Lead atualizarLead(Lead lead){
        Optional<Lead> leadOptional = buscarPorId(lead.getId());

        if(leadOptional.isPresent()){
            Lead leadData = leadOptional.get();
            if (lead.getNome() == null){
                lead.setNome(leadData.getNome());
            }
            if (lead.getEmail() == null){
                lead.setEmail(leadData.getEmail());
            }
            if(lead.getTipoDeLead() == null){
                lead.setTipoDeLead(leadData.getTipoDeLead());
            }
        }
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }
}
