package com.br.leadcollector.LeadCollector.repositories;

import com.br.leadcollector.LeadCollector.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

    Usuario findByEmail(String email);

}
