package com.br.leadcollector.LeadCollector.repositories;

import com.br.leadcollector.LeadCollector.models.Lead;
import org.springframework.data.repository.CrudRepository;

public interface LeadRepository extends CrudRepository<Lead, Integer> {

}
