package com.br.leadcollector.LeadCollector.repositories;

import com.br.leadcollector.LeadCollector.models.Produto;
import org.springframework.data.repository.CrudRepository;

public interface ProdutoRepository extends CrudRepository<Produto, Integer> {
}
