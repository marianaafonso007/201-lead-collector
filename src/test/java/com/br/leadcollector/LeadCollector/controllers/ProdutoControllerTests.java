package com.br.leadcollector.LeadCollector.controllers;

import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.security.JWTUtil;
import com.br.leadcollector.LeadCollector.services.ProdutoService;
import com.br.leadcollector.LeadCollector.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(ProdutoController.class)
@Import(JWTUtil.class)
public class ProdutoControllerTests {
    @MockBean
    private UsuarioService usuarioService;
    @MockBean
    ProdutoService produtoService;
    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();
    Produto produto;
    int entrada = 1;

    @BeforeEach
    public void iniciar(){
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda boa vista. ");
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarProduto() throws Exception{
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoService.buscarProdutoPorId(entrada)).thenReturn(retorno);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Café")));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarProdutoError() throws Exception{
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoService.buscarProdutoPorId(entrada)).thenReturn(retorno);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos/10")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarTodosLead() throws Exception{
        Iterable<Produto> produtosIterable = Arrays.asList(produto);
        Mockito.when(produtoService.buscarTodosProdutos()).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(produtosIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/produtos")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Café")));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarCadastroDeProduto() throws Exception{

        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(produtoService.salvarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.post("/produtos")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Café")));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarAtualizarProduto() throws Exception{
        Optional<Produto> retorno = Optional.of(produto);
        Produto produtoComAlteracao = new Produto();
        produtoComAlteracao = produto;
        produtoComAlteracao.setNome("Chá mate");
        Mockito.when(produtoService.atualizarProduto(Mockito.any(Produto.class))).thenReturn(produto);

        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.put("/produtos/1")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Chá mate")));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarDeletarProduto() throws Exception{
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoService.buscarProdutoPorId(entrada)).thenReturn(retorno);
        String json = mapper.writeValueAsString(produto);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarDeletarProdutoError() throws Exception{
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoService.buscarProdutoPorId(entrada)).thenReturn(retorno);

        mockMvc.perform(MockMvcRequestBuilders.delete("/produtos/100"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
