package com.br.leadcollector.LeadCollector.controllers;

import com.br.leadcollector.LeadCollector.enums.TipoDeLead;
import com.br.leadcollector.LeadCollector.models.Lead;
import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.security.JWTUtil;
import com.br.leadcollector.LeadCollector.services.LeadService;
import com.br.leadcollector.LeadCollector.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadController.class)
@Import(JWTUtil.class)
public class LeadControllerTests {
    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    LeadService leadService;
    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();

    Lead lead;
    Produto produto;
    int entrada = 1;

    @BeforeEach
    public void iniciar(){
        lead = new Lead();
        lead.setNome("Mariana Felix");
        lead.setEmail("mariana@teste.com");
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda boa vista. ");

        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarCadastroDeLead() throws Exception{

        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarLead() throws Exception{
        lead.setId(1);
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(entrada)).thenReturn(retorno);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarLeadError() throws Exception{
        lead.setId(1);
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(entrada)).thenReturn(retorno);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/10")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarBuscarTodosLead() throws Exception{
        lead.setId(1);
        Iterable<Produto> produtosIterable = Arrays.asList(produto);
        Iterable<Lead> leadsIterable = Arrays.asList(lead);

        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadsIterable);

        String json = mapper.writeValueAsString(leadsIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarAtualizarLead() throws Exception{
        lead.setId(1);
        Optional<Lead> retorno = Optional.of(lead);
        Lead leadComAlteracao = new Lead();
        leadComAlteracao = lead;
        leadComAlteracao.setNome("Luciana Maria Felix");
        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class))).thenReturn(lead);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Luciana Maria Felix")));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarDeletarLead() throws Exception{
        lead.setId(1);
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(entrada)).thenReturn(retorno);
        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    @WithMockUser(username = "marianafelix@gmail.com", password = "senha")
    public void testarDeletarLeadError() throws Exception{
        lead.setId(1);
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(entrada)).thenReturn(retorno);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/100"))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }


}
