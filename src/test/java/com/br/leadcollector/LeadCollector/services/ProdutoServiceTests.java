package com.br.leadcollector.LeadCollector.services;

import com.br.leadcollector.LeadCollector.enums.TipoDeLead;
import com.br.leadcollector.LeadCollector.models.Lead;
import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.repositories.LeadRepository;
import com.br.leadcollector.LeadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {
    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;
    int entrada = 1;

    @BeforeEach
    public void inicializar(){
        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(10.00);
        produto.setDescricao("Café da fazenda boa vista. ");
    }

    @Test
    public void testarBuscarProdutoPorId(){
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoRepository.findById(entrada)).thenReturn(retorno);

        Optional<Produto> produtoObjeto = produtoService.buscarProdutoPorId(1);

        Assertions.assertEquals(retorno, produtoObjeto);
    }

    @Test
    public void testarSalvarProduto(){
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);

        Produto produtoObjeto = produtoService.salvarProduto(produto);

        Assertions.assertEquals(produto, produtoObjeto);
    }

    @Test
    public void testarBuscarTodosProdutos(){
        Iterable<Produto> produtosIterable = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtosIterable);

        Iterable<Produto> produtosObjeto = produtoService.buscarTodosProdutos();

        Assertions.assertEquals(produtosIterable, produtosObjeto);
    }

    @Test
    public void testarAtualizarProduto(){
        Produto produtoAtualizado = new Produto();
        produtoAtualizado.setId(1);
        produtoAtualizado.setNome("Suco de Laranja");
        Produto produtoComAlteracao = new Produto();
        produtoComAlteracao = produto;
        produtoComAlteracao.setNome("Suco de Laranja");;
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoRepository.findById(entrada)).thenReturn(retorno);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoComAlteracao);

        Produto produtoObjeto = produtoService.atualizarProduto(produtoAtualizado);

        Assertions.assertEquals(produtoAtualizado.getNome(), produtoObjeto.getNome());
    }

    @Test
    public void testarAtualizarProduto2(){
        Produto produtoAtualizado = new Produto();
        produtoAtualizado.setId(1);
        produtoAtualizado.setDescricao("Acabou o Café");
        Produto produtoComAlteracao = new Produto();
        produtoComAlteracao = produto;
        produtoComAlteracao.setDescricao("Acabou o Café");
        Optional<Produto> retorno = Optional.of(produto);
        Mockito.when(produtoRepository.findById(entrada)).thenReturn(retorno);
        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoComAlteracao);

        Produto produtoObjeto = produtoService.atualizarProduto(produtoAtualizado);

        Assertions.assertEquals(produtoAtualizado.getDescricao(), produtoObjeto.getDescricao());
    }

    @Test
    public void testarDeletarProduto(){
        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository, Mockito.times(1)).delete(Mockito.any(Produto.class));
    }

}
