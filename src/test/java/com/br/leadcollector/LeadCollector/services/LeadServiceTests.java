package com.br.leadcollector.LeadCollector.services;

import com.br.leadcollector.LeadCollector.enums.TipoDeLead;
import com.br.leadcollector.LeadCollector.models.Lead;
import com.br.leadcollector.LeadCollector.models.Produto;
import com.br.leadcollector.LeadCollector.repositories.LeadRepository;
import com.br.leadcollector.LeadCollector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTests {

    @MockBean
    LeadRepository leadRepository;

    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;
    int entrada = 1;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("mariana@teste.com");
        lead.setNome("Marianaaaaaaaa");
        lead.setProdutos(Arrays.asList(new Produto()));
    }

    @Test
    public void testarSalvarLead(){
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);

        Lead leadObjeto = leadService.salvarLead(lead);

        Assertions.assertEquals(lead, leadObjeto);
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadRepository.findById(entrada)).thenReturn(retorno);

        Optional<Lead> leadObjeto = leadService.buscarPorId(1);

        Assertions.assertEquals(retorno, leadObjeto);
    }

    @Test
    public void testarAtualizarLead(){
        Lead leadAtualizado = new Lead();
        leadAtualizado.setId(1);
        leadAtualizado.setNome("Mariana Afonso Felix");
        Lead leadComAlteracao = new Lead();
        leadComAlteracao = lead;
        leadComAlteracao.setNome("Mariana Afonso Felix");
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadRepository.findById(entrada)).thenReturn(retorno);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadComAlteracao);

        Lead leadObjeto = leadService.atualizarLead(leadAtualizado);

        Assertions.assertEquals(leadAtualizado.getNome(), leadObjeto.getNome());
    }

    @Test
    public void testarAtualizarLead2(){
        Lead leadAtualizado = new Lead();
        leadAtualizado.setId(1);
        leadAtualizado.setTipoDeLead(TipoDeLead.ORGANICO);
        Lead leadComAlteracao = new Lead();
        leadComAlteracao = lead;
        leadComAlteracao.setTipoDeLead(TipoDeLead.ORGANICO);
        Optional<Lead> retorno = Optional.of(lead);
        Mockito.when(leadRepository.findById(entrada)).thenReturn(retorno);
        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadComAlteracao);

        Lead leadObjeto = leadService.atualizarLead(leadAtualizado);

        Assertions.assertEquals(leadAtualizado.getTipoDeLead(), leadObjeto.getTipoDeLead());
    }

    @Test
    public void testarDeletarLead(){
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
    }

}
